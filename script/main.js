const burger = document.body.querySelector('.burger-menu');

burger.addEventListener('click', () => {
    document.body.querySelector('.header__nav-bar').classList.toggle('header__nav-bar_hidden');
    burger.classList.toggle('burger-menu_crossed');
})